# Boilerplates

Personal project boilerplates.

Largely contains project templates that integrate with
* NixOS
* nvim + coc.nvim + coc language plugins
* direnv

## Usage
```
# Copy language-specific foler.
cp -r python DIR
```
