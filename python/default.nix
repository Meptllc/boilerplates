{ pkgs ? import <nixpkgs> {}
, pythonPackages ? pkgs.python37Packages }:

let
  # f would be what would go in nixpkgs. Which explains the strange style.
  f =
    { buildPythonApplication
    , pytest
    , click
    }:

    buildPythonApplication rec {
      version = "master";
      pname = "myproject";
      src = ./.;

      doCheck = false;
      checkInputs = [ pytest ];
      propagatedBuildInputs = [
        click
      ];
    };
  drv = pythonPackages.callPackage f {};
in
  drv
