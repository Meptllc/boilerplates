"""Module setup."""

import os
import pkg_resources
import setuptools

PACKAGE_NAME = "myproject"


with open("README.md", "r") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name=PACKAGE_NAME,
        version="0.0.1",
        author="Christopher Chin",
        author_email="ctchin13@gmail.com",
        packages=setuptools.find_packages(),
        python_requires=">=3.7.7",
        description="My project",
        long_description=long_description,
        long_description_content_type="text/markdown",

        install_requires=[
            str(r)
            for r in pkg_resources.parse_requirements(
                open(os.path.join(os.path.dirname(__file__), "requirements.txt"))
            )
        ],
        entry_points={
            'console_scripts': [
                f'{PACKAGE_NAME}={PACKAGE_NAME}.cli:cli'
            ],
        },
    )
